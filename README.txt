Booking.com API

Usage:
* enable module
* in your custom module add code like below:

<code>
 $booking = new Booking();
 $parameters = array(
   'rows' => 3,
   'countrycodes' => 'pl'
 );
 $data = $booking->getCities($parameters);
</code>

Functions list: http://xml.booking.com/affiliates/documentation/xml_functions.html

Test account:
username: w3bm4rket
password: Gro3p
