<?php

interface Connector {

  /**
   * Function connects to Booking API and get data.
   *
   * @param $functionName
   *   Function name from http://xml.booking.com/affiliates/documentation/xml_functions.html
   * @param $parameters
   *   Parameters for function.
   *
   * @return
   *   Array of data.
   */
  public function getData($functionName, $parameters);
}
