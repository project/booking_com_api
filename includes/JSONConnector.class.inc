<?php

require_once 'Connector.class.inc';

class JSONConnector implements Connector {

  public function __construct() {
  }

  /**
   * Get data from Booking.com or from cache.
   *
   * @param type $functionName
   * @param type $parameters
   * @return type
   */
  public function getData($functionName, $parameters) {
    // Build URL.
    $protocol = variable_get('booking_com_api_protocol', 'http://');
    $base_url_addr = variable_get('booking_com_api_url', 'distribution-xml.booking.com/');
    $username = variable_get('booking_com_api_username', '');
    $password = variable_get('booking_com_api_password', '');
    if ($username != '' && $password != '') {
      $up_string =  $username . ':' . $password . '@' ;
    }
    else {
      $up_string = '';
    }
    $base_url = $protocol . $up_string . $base_url_addr . 'json/';
    $param_string = '?';

    // Convert parameters to GET string.
    foreach ($parameters as $key => $value) {
      $param_string .= $key . '=' . $value . '&';
    }
    // Remove unused chars at end of string.
    $param_string = rtrim($param_string, '&');
    $param_string = rtrim($param_string, '?');

    // Get remote data from XML file.
    $url = $base_url . $functionName . $param_string;
    $url_hash_cache_id = sha1($url);
    // Get data from cache table or from remote server.
    // @TODO - skip noncachable $functionName
    if ($cache_data = cache_get($url_hash_cache_id, BOOKING_COM_API_CACHE_TABLE)) {
      return $cache_data->data;
    }
    else {
      // Get the data from the service
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return data instead of display to std out

      $json_data = curl_exec($ch);
      $data = json_decode($json_data);

      $cache_expire = variable_get('booking_com_api_cache_expire', CACHE_TEMPORARY);
      cache_set($url_hash_cache_id, $data, BOOKING_COM_API_CACHE_TABLE, $cache_expire);
      return $data;
    }
  }
}
