<?php

require_once 'Connector.class.inc';

class XMLConnector implements Connector {

  public function __construct() {
  }

  /**
   * Convert SimpleXMLElement to array.
   * Function copied from some website ;)
   *
   * @param $xml
   *   SimpleXMLElement variable.
   *
   * @return
   *   Array variable.
   */
  function simplexml2array($xml) {
    if (get_class((object) $xml) == 'SimpleXMLElement') {
      $attributes = $xml->attributes();
      foreach ($attributes as $k => $v) {
        if ($v)
          $a[$k] = (string) $v;
      }
      $x = $xml;
      $xml = get_object_vars($xml);
    }
    if (is_array($xml)) {
      if (count($xml) == 0)
        return (string) $x; // for CDATA
      foreach ($xml as $key => $value) {
        $r[$key] = $this->simplexml2array($value);
      }
      if (isset($a))
        $r['@attributes'] = $a;    // Attributes
      return $r;
    }
    return (string) $xml;
  }

  /**
   * Get data from Booking.com or from cache.
   *
   * @param type $functionName
   * @param type $parameters
   * @return type
   */
  public function getData($functionName, $parameters) {
    // Build URL.
    $protocol = variable_get('booking_com_api_protocol', 'http://');
    $base_url_addr = variable_get('booking_com_api_url', 'distribution-xml.booking.com/xml/');
    $username = variable_get('booking_com_api_username', '');
    $password = variable_get('booking_com_api_password', '');
    if ($username != '' && $password != '') {
      $up_string =  $username . ':' . $password . '@' ;
    }
    else {
      $up_string = '';
    }
    $base_url = $protocol . $up_string . $base_url_addr;
    $param_string = '?';

    // Convert parameters to GET string.
    foreach ($parameters as $key => $value) {
      $param_string .= $key . '=' . $value . '&';
    }
    // Remove unused chars at end of string.
    $param_string = rtrim($param_string, '&');
    $param_string = rtrim($param_string, '?');

    // Get remote data from XML file.
    $url = $base_url . $functionName . $param_string;
    //drupal_set_message('URL: ' . $url); // debug.
    $url_hash_cache_id = sha1($url);
    // Get data from cache table or from remote server.
    // @TODO - skip noncachable $functionName
    if ($cache_data = cache_get($url_hash_cache_id, BOOKING_COM_API_CACHE_TABLE)) {
      return $cache_data->data;
    }
    else {
      $simplexml_data = simplexml_load_file($url);
      //drupal_set_message('cache XML: <pre>' . print_r($simplexml_data, TRUE) . '</pre>' . $url_hash_cache_id);  // debug.
      $data = $this->simplexml2array($simplexml_data);
      //drupal_set_message('cache XML array: <pre>' . print_r($data, TRUE) . '</pre>');  // debug.
      $cache_expire = variable_get('booking_com_api_cache_expire', CACHE_TEMPORARY);
      cache_set($url_hash_cache_id, $data, BOOKING_COM_API_CACHE_TABLE, $cache_expire);
      return $data;
    }
  }

}
