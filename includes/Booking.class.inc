<?php

class Booking {

  protected $connector;

  public function __construct() {
    // Select connection type: XML or XML-RPC.
    $connectorDrupalSettings = variable_get('booking_com_api_connector', 'XML'); // XML or XML-RPC
    $connectorObject = new stdClass();
    switch ($connectorDrupalSettings) {
      case 'XML':
        require_once 'XMLConnector.class.inc';
        $connectorObject = new XMLConnector();
        break;
      case 'JSON':
        require_once 'JSONConnector.class.inc';
        $connectorObject = new JSONConnector();
        break;
      case 'XML-RPC':
        require_once 'XMLRPCConnector.class.inc';
        $connectorObject = new XMLRPCConnector();
        break;
      case 'TEST':
        require_once 'TESTConnector.class.inc';
        $connectorObject = new XMLRPCConnector();
        break;
    }
    $this->connector = $connectorObject;
  }

  /**
   * Returns a list of hotel chains (in English).
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getchaintypes.html
   *
   * @return
   *   Array.
   */
  public function getChainTypes($parameters) {
    return $this->connector->getData('bookings.getChainTypes', $parameters);
  }

  /**
   * Returns chains per hotel.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getchains.html
   *
   * @return
   *   Array.
   */
  public function getChains($parameters) {
    return $this->connector->getData('bookings.getChains', $parameters);
  }

  /**
   * Returns ids from hotels where *something* has been added / changed / removed
   * (including closed hotels) since a certain date. These hotels should be synced
   * completely, so that deleted objects & hotels will also be removed from your
   * local database. Can be used periodically in addition to the {last_change}
   * parameter of several functions.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getchangedhotels.html
   *
   * @return
   *   Array.
   */
  public function getChangedHotels($parameters) {
    return $this->connector->getData('bookings.getChangedHotels', $parameters);
  }

  /**
   * Returns list of cities (with some translations).
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getcities.html
   *
   * @return
   *   Array.
   */
  public function getCities($parameters) {
    return $this->connector->getData('bookings.getCities', $parameters);
  }

  /**
   * Returns countrycodes and their translated names. If a country name is not available in the requested language it's English name will be returned instead.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getcountries.html
   *
   * @return
   *   Array.
   */
  public function getCountries($parameters) {
    return $this->connector->getData('bookings.getCountries', $parameters);
  }

  /**
   * Returns a list of credit cards (in English). Only credit cards with
   *  {bookable} set to 1, can be used in bookings.processBooking.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getcreditcardtypes.html
   *
   * @return
   *   Array.
   */
  public function getCreditcardTypes($parameters) {
    return $this->connector->getData('bookings.getCreditcardTypes', $parameters);
  }

  /**
   * Returns credit cards accepted at the hotel. Note: not all accepted credit cards
   *  can be used in bookings.processBooking (see also bookings.getCreditcardTypes{bookable}).
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getcreditcards.html
   *
   * @return
   *   Array.
   */
  public function getCreditcards($parameters) {
    return $this->connector->getData('bookings.getCreditcards', $parameters);
  }

  /**
   * Returns all the districts where Bookings offers hotels.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getdistricts.html
   *
   * @return
   *   Array.
   */
  public function getDistricts($parameters) {
    return $this->connector->getData('bookings.getDistricts', $parameters);
  }

  /**
   * Returns a list of district_id and hotel_id pairs. This function expects any
   *  of the following search parameters: countrycodes, city_ids, district_ids
   *  and last_change.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getdistricthotels.html
   *
   * @return
   *   Array.
   */
  public function getDistrictHotels($parameters) {
    return $this->connector->getData('bookings.getDistrictHotels', $parameters);
  }

  /**
   * Returns facility types.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getfacilitytypes.html
   *
   * @return
   *   Array.
   */
  public function getFacilityTypes($parameters) {
    return $this->connector->getData('bookings.getFacilityTypes', $parameters);
  }

  /**
   * Returns hotel description photo's.

   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethoteldescriptionphotos.html
   *
   * @return
   *   Array.
   */
  public function getHotelDescriptionPhotos($parameters) {
    return $this->connector->getData('bookings.getHotelDescriptionPhotos', $parameters);
  }

  /**
   * Returns hotel descriptions.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethotdesctrans.html
   *
   * @return
   *   Array.
   */
  public function getHotelDescriptionTranslations($parameters) {
    return $this->connector->getData('bookings.getHotelDescriptionTranslations', $parameters);
  }

  /**
   * Returns hotel description types.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethoteldescriptiontypes.html
   *
   * @return
   *   Array.
   */
  public function getHotelDescriptionTypes($parameters) {
    return $this->connector->getData('bookings.getHotelDescriptionTypes', $parameters);
  }

  /**
   * Returns hotel facilities.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethotelfacilities.html
   *
   * @return
   *   Array.
   */
  public function getHotelFacilities($parameters) {
    return $this->connector->getData('bookings.getHotelFacilities', $parameters);
  }

  /**
   * Returns hotel facility types.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethotelfacilitytypes.html
   *
   * @return
   *   Array.
   */
  public function getHotelFacilityTypes($parameters) {
    return $this->connector->getData('bookings.getHotelFacilityTypes', $parameters);
  }

  /**
   * Returns hotel logo photo's.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethotellogophotos.html
   *
   * @return
   *   Array.
   */
  public function getHotelLogoPhotos($parameters) {
    return $this->connector->getData('bookings.getHotelLogoPhotos', $parameters);
  }

  /**
   * Returns hotel photo's.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethotelphotos.html
   *
   * @return
   *   Array.
   */
  public function getHotelPhotos($parameters) {
    return $this->connector->getData('bookings.getHotelPhotos', $parameters);
  }

  /**
   * Returns hotel name/address/city translations.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethoteltranslations.html
   *
   * @return
   *   Array.
   */
  public function getHotelTranslations($parameters) {
    return $this->connector->getData('bookings.getHotelTranslations', $parameters);
  }

  /**
   * Returns hotel types.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethoteltypes.html
   *
   * @return
   *   Array.
   */
  public function getHotelTypes($parameters) {
    return $this->connector->getData('bookings.getHotelTypes', $parameters);
  }

  /**
   * Returns hotel details.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_gethotels.html
   *
   * @return
   *   Array.
   */
  public function getHotels($parameters) {
    return $this->connector->getData('bookings.getHotels', $parameters);
  }

  /**
   * Returns all the regions where Bookings offers hotels.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getregions.html
   *
   * @return
   *   Array.
   */
  public function getRegions($parameters) {
    return $this->connector->getData('bookings.getRegions', $parameters);
  }

  /**
   * Returns Hotels and its assosiated regional information.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getregionhotels.html
   *
   * @return
   *   Array.
   */
  public function getRegionHotels($parameters) {
    return $this->connector->getData('bookings.getRegionHotels', $parameters);
  }

  /**
   * Returns room facilities.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getroomfacilities.html
   *
   * @return
   *   Array.
   */
  public function getRoomFacilities($parameters) {
    return $this->connector->getData('bookings.getRoomFacilities', $parameters);
  }

  /**
   * Returns room facility types.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getroomfacilitytypes.html
   *
   * @return
   *   Array.
   */
  public function getRoomFacilityTypes($parameters) {
    return $this->connector->getData('bookings.getRoomFacilityTypes', $parameters);
  }

  /**
   * Returns room photos.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getroomphotos.html
   *
   * @return
   *   Array.
   */
  public function getRoomPhotos($parameters) {
    return $this->connector->getData('bookings.getRoomPhotos', $parameters);
  }

  /**
   * Returns room translations.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getroomtranslations.html
   *
   * @return
   *   Array.
   */
  public function getRoomTranslations($parameters) {
    return $this->connector->getData('bookings.getRoomTranslations', $parameters);
  }

  /**
   * Returns room types.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getroomtypes.html
   *
   * @return
   *   Array.
   */
  public function getRoomTypes($parameters) {
    return $this->connector->getData('bookings.getRoomTypes', $parameters);
  }

  /**
   * Returns rooms.
   *
   * @param $parameters
   *   Array of parameters.
   *   List of parameters: http://xml.booking.com/affiliates/documentation/xml_getrooms.html
   *
   * @return
   *   Array.
   */
  public function getRooms($parameters) {
    return $this->connector->getData('bookings.getRooms', $parameters);
  }

}