<?php
/**
 * @file
 * Settings.
 */

/**
 * Settings form.
 *
 * @return
 *   Form array.
 */
function booking_com_api_settings() {
  $form = array();
  $form['booking_com_api_connector'] = array(
    '#type' => 'radios',
    '#title' => t('Connection type'),
    '#default_value' => variable_get('booking_com_api_connector', 'XML'),
    '#description' => t('Choose your connection type.'),
    '#required' => TRUE,
    '#options' => array(
      'XML' => t('XML'),
      'JSON' => t('JSON'),
      //'XML-RPC' => t('XML-RPC'),
    ),
  );
  $form['booking_com_api_protocol'] = array(
    '#type' => 'radios',
    '#title' => t('Connection protocol'),
    '#default_value' => variable_get('booking_com_api_protocol', 'https://'),
    '#description' => t('Choose your API protocol.'),
    '#required' => TRUE,
    '#options' => array(
      'http://' => 'http://',
      'https://' => 'https://',
    ),
  );
  $form['booking_com_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Service URL'),
    '#required' => TRUE,
    '#default_value' => variable_get('booking_com_api_url', 'distribution-xml.booking.com/'),
  );
  $form['booking_com_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Booking.com API username'),
    '#required' => TRUE,
    '#default_value' => variable_get('booking_com_api_username', ''),
  );
  $form['booking_com_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Booking.com API password'),
    '#required' => TRUE,
    '#default_value' => variable_get('booking_com_api_password', ''),
  );

  /*
  $form['booking_com_api_cache_expire'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache expire'),
    '#description' => t('Value in seconds'),
    '#required' => TRUE,
    '#default_value' => variable_get('booking_com_api_cache_expire'),
  );
   */
  return system_settings_form($form);
}
